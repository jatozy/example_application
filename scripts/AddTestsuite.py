import os
from string import Template
import zipfile
import argparse

# Read the Input Arguments
parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name',             # The Argument name is name
            action="store",                     # Store the arg to a variable
            dest="title",                       # Variable title
            help="The name of the Testsuite",   # Help text
            default="NewTestSuite")             # Default arg value
parser.add_argument('-d', '--directory',
                  action="store",
                  dest="directory",
                  help="The directory where the test will be created",
                  default="tests")

options = parser.parse_args()
# sets the preferred title according to the command line args
title = options.title
# sets the directory where the test will be created
testDirectory = os.path.join(os.getcwd(), options.directory)
# create the directory if it doesn't exist
if not os.path.exists(testDirectory):
    os.makedirs(testDirectory)

# The name of the Template folder contained in the zip
templateFolderName = os.path.join(testDirectory, "TestTemplate")

# Creates a dictionary that defines which template variables should be replaced with what
# at the moment only the string "Template" is replaced, but that might change in the future
dictionary = { 'Template':title }

# Extract the template CMakeLists.txt, Template.h and Template.cxx from the zip file
with zipfile.ZipFile(os.path.join(os.path.dirname(__file__), 'AddTestsuite_TestTemplate.zip'), "r") as z:
    z.extractall(testDirectory)

# substitute the variables in the template files with the proper name
for filename in os.listdir(templateFolderName):	
    fullFilePath = os.path.join(templateFolderName, filename)   # full path to the file
    filein = open(fullFilePath)
    #read it
    src = Template(filein.read())
    #do the substitution
    result = src.safe_substitute(dictionary)
    filein.close()

    # Write the result of the substition back to the file,
    # effectively replacing the content with it
    # TODO the two open calls could probably be combined
    with open(fullFilePath, "w") as f:
        f.write(result)

    # Rename the C++ files to have the proper name
    if filename.startswith("Template"):
        fullFilePathNew = os.path.join(templateFolderName, title + filename[8:])    # full path to the new file
        os.rename(fullFilePath, fullFilePathNew)    # Replace Template.h/.cxx with title.h/.cxx

# Rename the Testfolder itself
newFolderTitle = os.path.join(testDirectory, title)
os.rename(templateFolderName, newFolderTitle)

# Open the /Tests/CMakeLists.txt and append add_subdirectory(created project)
addSubdirectoryString = "\nADD_SUBDIRECTORY(" + title + ") # automatically created with AddTestsuite.py"
cmakeFilePath = os.path.join(testDirectory, "CMakeLists.txt")
# Touch the CMakeLists.txt. This will create an empty file if none exists
open(cmakeFilePath, 'a').close()
with open(cmakeFilePath, 'a') as file:
    file.write(addSubdirectoryString)
