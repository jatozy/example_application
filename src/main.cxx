#include <iostream>

#include <comp1.h>
#include <comp2.h>

int main(void)
{
    std::cout<<"start application"<<std::endl;
    comp1 a;
    comp2 b;
    std::cout<<"end application"<<std::endl;

    return 0;
}
