if(EXISTS "${PROJECT_SOURCE_DIR}/.gitmodules")
message(STATUS "Updating submodules to their latest/fixed versions")
message(STATUS "(this can take a while, please be patient)")

### set the direcory where the submodules live
set(GIT_SUBMODULES_DIRECTORY extern)

### set the directory names of the submodules
set(GIT_SUBMODULES comp1 comp2)

### set each submodules's commit or tag that is to be checked out
### (leave empty if you want master)
set(GIT_SUBMODULE_VERSION_comp1 0.3.0)
set(GIT_SUBMODULE_VERSION_comp2 0.5.0)

### First, get all submodules in
execute_process(
    COMMAND             git submodule update --init --recursive
    WORKING_DIRECTORY   ${PROJECT_SOURCE_DIR}
)
execute_process(
    COMMAND             git submodule update --recursive --remote --force
    WORKING_DIRECTORY   ${PROJECT_SOURCE_DIR}
)

### Then, checkout each submodule to the specified commit
# Note: Execute separate processes here, to make sure each one is run,
# should one crash (because of branch not existing, this, that ... whatever)
foreach(GIT_SUBMODULE ${GIT_SUBMODULES})

    if( "${GIT_SUBMODULE_VERSION_${GIT_SUBMODULE}}" STREQUAL "" )
        message(STATUS "no specific version given for submodule ${GIT_SUBMODULE}, checking out master")
        set(GIT_SUBMODULE_VERSION_${GIT_SUBMODULE} "master")
    endif()

#    message(STATUS "execute >>git fetch --all --tags --prune<< in dir >>${PROJECT_SOURCE_DIR}/${GIT_SUBMODULES_DIRECTORY}/${GIT_SUBMODULE}<<")
    execute_process(
        COMMAND             git fetch --all --tags --prune
        WORKING_DIRECTORY   ${PROJECT_SOURCE_DIR}/${GIT_SUBMODULES_DIRECTORY}/${GIT_SUBMODULE}
    )

    message(STATUS "checking out ${GIT_SUBMODULE}'s commit/tag ${GIT_SUBMODULE_VERSION_${GIT_SUBMODULE}}")
#    message(STATUS "execute >>git checkout tags/${GIT_SUBMODULE_VERSION_${GIT_SUBMODULE}}<< in dir >>${PROJECT_SOURCE_DIR}/${GIT_SUBMODULES_DIRECTORY}/${GIT_SUBMODULE}<<")
    execute_process(
        COMMAND             git checkout tags/${GIT_SUBMODULE_VERSION_${GIT_SUBMODULE}}
        WORKING_DIRECTORY   ${PROJECT_SOURCE_DIR}/${GIT_SUBMODULES_DIRECTORY}/${GIT_SUBMODULE}
    )

endforeach(${GIT_SUBMODULE})

endif()
